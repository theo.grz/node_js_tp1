const jwt = require("jsonwebtoken");

function authenticateToken(req, res, next) {
  const tokenHeader = req.headers.authorization;
  if (!tokenHeader) {
    return res.status(401).json({ message: "Token non fourni" });
  }

  try {
    const token = tokenHeader;
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.user = decoded;
    next();
  } catch (error) {
    console.error("Erreur de vérification :", error);
    res.status(401).json({ message: "Token invalide" });
  }
}

module.exports = authenticateToken;
