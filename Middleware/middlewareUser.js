const jwt = require("jsonwebtoken");

function verifyAdmin(req, res, next) {
  const authHeader = req.headers["authorization"];
  const token = authHeader;

  if (!token) {
    return res.status(403).send("Un token est requis pour l'authentification");
  }

  try {
    const secretKey = process.env.JWT_SECRET;
    const decoded = jwt.verify(token, secretKey);

    if (decoded.is_admin !== "admin" && decoded.is_admin !== "journaliste") {
      return res
        .status(403)
        .send("Accès refusé. Réservé aux administrateurs et journalistes.");
    }

    req.user = decoded;
  } catch (err) {
    return res.status(401).send("Token invalide");
  }

  next();
}

module.exports = verifyAdmin;
