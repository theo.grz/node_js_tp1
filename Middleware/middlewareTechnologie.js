const jwt = require("jsonwebtoken");

function authenticateToken(req, res, next) {
  try {
    const tokenHeader = req.headers.authorization;
    if (!tokenHeader)
      return res.status(401).json({ message: "Token non fourni" });

    const token = tokenHeader;
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    if (decoded.is_admin !== "admin") {
      return res.status(401).json({ message: "Accès refusé" });
    }

    next();
  } catch (error) {
    console.error("Erreur de vérification :", error);
    res.status(401).json({ message: "Non autorisé" });
  }
}

module.exports = authenticateToken;
