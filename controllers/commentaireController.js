const db = require("../database");

// Affiche tous les commentaires
exports.getCommentaire = (req, res) => {
  const sql = "SELECT * FROM commentaire";
  db.query(sql, (error, results) => {
    if (error) {
      return res
        .status(500)
        .send("Erreur lors de la récupération des commentaires");
    }
    res.json(results);
  });
};

//afficher tout les commentaire dune technologie
exports.getCommentaireTechnologie = (req, res) => {
  const { id } = req.params;
  const sql = "SELECT * FROM commentaire WHERE technologie_id = ?";
  db.query(sql, [id], (error, results) => {
    if (error)
      return res
        .status(500)
        .send("Erreur lors de la récupération des commentaires");
    res.json(results);
  });
};

//afficher tout les message ecrit par une personne
exports.getCommentaireUser = (req, res) => {
  const { id } = req.params;
  const sql = "SELECT * FROM commentaire WHERE utilisateur_id = ?";
  db.query(sql, [id], (error, results) => {
    if (error)
      return res
        .status(500)
        .send("Erreur lors de la récupération des commentaires");
    res.json(results);
  });
};

//tout les commentaire anterieur a la date ciblé
exports.getCommentaireAvant = (req, res) => {
  const { date } = req.params;
  const sql = "SELECT * FROM commentaire WHERE date_creation_commentaire < ?";
  db.query(sql, [date], (error, results) => {
    if (error)
      return res
        .status(500)
        .send("Erreur lors de la récupération des commentaires");
    res.json(results);
  });
};

//ecrire un commentaire
exports.postCommentaire = (req, res) => {
  const { contenu, utilisateur_id, technologie_id } = req.body;
  const sql =
    "INSERT INTO commentaire (contenu, utilisateur_id, technologie_id, date_creation_commentaire) VALUES (?, ?, ?, NOW())";
  db.query(sql, [contenu, utilisateur_id, technologie_id], (error, results) => {
    if (error)
      return res.status(500).send("Erreur lors de l'écriture du commentaire");
    res.status(201).send("Commentaire créé");
  });
};
