const db = require("../database");
const bcrypt = require("bcrypt");
const saltRounds = parseInt(process.env.SALT_ROUNDS);
const jwt = require("jsonwebtoken");

exports.getUtilisateurs = (req, res) => {
  db.query("SELECT * FROM utilisateur", (error, results) => {
    if (error) throw error;
    res.json(results);
  });
};

exports.postUtilisateur = async (req, res) => {
  try {
    const { nom, prenom, email, mdp } = req.body;
    const hashedMdp = await bcrypt.hash(mdp, saltRounds);

    const sql =
      "INSERT INTO utilisateur (nom, prenom, email, mdp) VALUES (?, ?, ?, ?)";
    db.query(sql, [nom, prenom, email, hashedMdp], (error, results) => {
      if (error) {
        console.error(error);
        return res
          .status(500)
          .send(
            "Erreur lors de la création de l'utilisateur : " + error.sqlMessage
          );
      }
      res.status(201).send("Utilisateur créé");
    });
  } catch (error) {
    console.error(error);
    res.status(500).send("Erreur lors de l'inscription");
  }
};

// verfier l'authentification de lutilisateur avec le jwtoken
exports.postConnexion = (req, res) => {
  const { email, mdp } = req.body;
  const sql = "SELECT * FROM utilisateur WHERE email = ?";

  db.query(sql, [email], async (error, results) => {
    if (error) {
      console.error(error);
      return res.status(500).send("Erreur lors de la connexion");
    }

    if (results.length === 0) {
      return res.status(401).send("Aucun utilisateur trouvé avec cet email");
    }

    const utilisateur = results[0];
    const match = await bcrypt.compare(mdp, utilisateur.mdp);

    if (match) {
      const secretKey = process.env.JWT_SECRET;
      const token = jwt.sign(
        { email: utilisateur.email, is_admin: utilisateur.is_admin },
        secretKey,
        { expiresIn: "1h" }
      );

      res.json({ message: "Connexion réussie", token });
    } else {
      res.status(401).send("Mot de passe incorrect");
    }
  });
};

//mettre a jour un utilisateur
exports.upUtilisateur = async (req, res) => {
  const { nom, prenom, email, mdp, is_admin } = req.body;
  const { id } = req.params;
  try {
    let hashedMdp = mdp;
    if (mdp) {
      hashedMdp = await bcrypt.hash(mdp, saltRounds);
    }

    const sql =
      "UPDATE utilisateur SET nom = ?, prenom = ?, email = ?, mdp = ?, is_admin = ? WHERE id = ?";
    db.query(
      sql,
      [nom, prenom, email, hashedMdp, is_admin, id],
      (error, results) => {
        if (error) {
          console.error(error);
          return res
            .status(500)
            .send("Erreur lors de la mise à jour de l'utilisateur");
        }
        res.status(200).send(`Utilisateur avec ID ${id} mis à jour`);
      }
    );
  } catch (error) {
    console.error(error);
    res.status(500).send("Erreur lors de la mise à jour de l'utilisateur");
  }
};

// supprimer un utilisateur
exports.deleteUtilisateur = (req, res) => {
  const { id } = req.params;
  const sql = "DELETE FROM utilisateur WHERE id = ?";
  db.query(sql, [id], (error, results) => {
    if (error)
      return res
        .status(500)
        .send("Erreur lors de la suppression de l'utilisateur");
    res.status(200).send(`Utilisateur avec ID ${id} supprimé`);
  });
};
