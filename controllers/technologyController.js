const db = require("../database");

exports.getTechnologies = (req, res) => {
  db.query("SELECT * FROM technologie", (error, results) => {
    if (error) throw error;
    res.json(results);
  });
};

exports.postTechnologie = (req, res) => {
  const { nom_techno, date_creation, nom_createur } = req.body;
  const sql =
    "INSERT INTO technologie (nom_techno, date_creation, nom_createur) VALUES (?, ?, ?)";
  db.query(sql, [nom_techno, date_creation, nom_createur], (error, results) => {
    if (error) {
      console.error(error);
      return res
        .status(500)
        .send("Erreur lors de la création de la technologie");
    }
    res.status(201).send("Technologie créée");
  });
};

exports.updateTechnologie = (req, res) => {
  const { nom_techno, date_creation, nom_createur } = req.body;
  const { id } = req.params;
  const sql =
    "UPDATE technologie SET nom_techno = ?, date_creation = ?, nom_createur = ? WHERE id = ?";
  db.query(
    sql,
    [nom_techno, date_creation, nom_createur, id],
    (error, results) => {
      if (error) {
        console.error(error);
        return res
          .status(500)
          .send("Erreur lors de la mise à jour de la technologie");
      }
      res.status(200).send(`Technologie avec ID ${id} mise à jour`);
    }
  );
};

exports.deleteTechnologie = (req, res) => {
  const { id } = req.params;
  const sql = "DELETE FROM technologie WHERE id = ?";
  db.query(sql, [id], (error, results) => {
    if (error) {
      console.error(error);
      return res
        .status(500)
        .send("Erreur lors de la suppression de la technologie");
    }
    res.status(200).send(`Technologie avec ID ${id} supprimée`);
  });
};
