document.addEventListener("DOMContentLoaded", function () {
  afficherUtilisateurs();
});

function afficherUtilisateurs() {
  fetch("http://localhost:8000/utilisateurs/utilisateurs")
    .then((response) => response.json())
    .then((data) => {
      const liste = document.getElementById("listeUtilisateurs");
      liste.innerHTML = "";
      data.forEach((user) => {
        liste.innerHTML += `<li>${user.nom} ${user.prenom} - ${user.email}</li>`;
      });
    });
}

function ajouterUtilisateur() {
  const nom = document.getElementById("nom").value;
  const prenom = document.getElementById("prenom").value;
  const email = document.getElementById("email").value;
  const mdp = document.getElementById("mdp").value;

  fetch("http://localhost:8000/utilisateurs/postUtilisateurs", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ nom, prenom, email, mdp }),
  })
    .then((response) => response.text())
    .then((data) => {
      alert(data);
      afficherUtilisateurs();
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}

function connexionUtilisateur() {
  const email = document.getElementById("emailConnexion").value;
  const mdp = document.getElementById("mdpConnexion").value;

  fetch("http://localhost:8000/utilisateurs/postConnexion", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ email, mdp }),
  })
    .then((response) => response.json())
    .then((data) => {
      if (data.token) {
        localStorage.setItem("token", data.token);
        alert("Connexion réussie");
        afficherCommentaire();
      } else {
        alert("Échec de la connexion");
      }
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}
function redirigerVersConnexion() {
  window.location.href = "http://localhost:8000/connexion.html";
}

function deconnexion() {
  localStorage.removeItem("token");
  window.location.href = "http://localhost:8000";
}

//fonction qui affiche la liste des commentaire uniquement pour les connecté
async function afficherCommentaire() {
  const listeTech = document.getElementById("listeCommentaire");
  try {
    const response = await fetch("/commentaire/commentaire", {
      method: "GET",
      headers: {
        Authorization: localStorage.getItem("token"),
      },
    });
    if (!response.ok) {
      throw new Error(`Erreur HTTP: ${response.status}`);
    }
    const data = await response.json();
    listeTech.innerHTML = "";
    data.forEach((tech) => {
      listeTech.innerHTML += `<li>${tech.message} - Créé par l'utilisateur id n°${tech.utilisateur_id} le ${tech.date_creation_commentaire}</li>`;
    });
  } catch (error) {
    console.error("Erreur lors de la récupération des commentaires:", error);
  }
}
