const express = require("express");
const router = express.Router();
const technologyController = require("../controllers/technologyController");
const authenticateToken = require("../Middleware/middlewareTechnologie");

router.get("/getTech", authenticateToken, technologyController.getTechnologies);
router.post(
  "/postTech",
  authenticateToken,
  technologyController.postTechnologie
);
router.put(
  "/upTech/:id",
  authenticateToken,
  technologyController.updateTechnologie
);
router.delete(
  "/deleteTech/:id",
  authenticateToken,
  technologyController.deleteTechnologie
);

module.exports = router;
