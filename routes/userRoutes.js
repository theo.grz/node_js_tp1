const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const verifyAdmin = require("../Middleware/middlewareUser");

//post un utilisateur
router.post("/postUtilisateurs", userController.postUtilisateur);

//verifie un admin
router.get("/admin/route", verifyAdmin, userController.getUtilisateurs);

//recuper tout les utilisateur
router.get("/utilisateurs", userController.getUtilisateurs);

//connexion user
router.post("/postConnexion", userController.postConnexion);

//modification user
router.put("/upUtilisateur/:id", userController.upUtilisateur);

//supprimer user
router.delete("/deleteUtilisateur/:id", userController.deleteUtilisateur);

module.exports = router;
