const express = require("express");
const router = express.Router();
const commentaireController = require("../controllers/commentaireController");
const authenticateTokenCommentaire = require("../Middleware/middlewareCommentaire");
const authenticateToken = require("../Middleware/middlewareCommentaireToken");

router.get(
  "/commentaire",
  authenticateToken,
  commentaireController.getCommentaire
);

router.get(
  "/technologies/:id/commentaires",
  authenticateToken,
  commentaireController.getCommentaireTechnologie
);
router.get(
  "/utilisateurs/:id/commentaires",
  authenticateToken,
  commentaireController.getCommentaireUser
);
router.get(
  "/commentaires/avant/:date",
  authenticateToken,
  commentaireController.getCommentaireAvant
);

router.post(
  "/commentaires",
  authenticateTokenCommentaire,
  commentaireController.postCommentaire
);

module.exports = router;
