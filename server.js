const express = require("express");
const app = express();
const cors = require("cors");
const path = require("path");

const userRoutes = require("./routes/userRoutes");
const commentaireRoutes = require("./routes/commentaireRoutes");
const technologyRoutes = require("./routes/technologyRoutes");

app.use(express.static(path.join(__dirname, "public")));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// routes user
app.use("/utilisateurs", userRoutes);

//routes commentaire
app.use("/commentaire", commentaireRoutes);

// Routes pour les technologies
app.use("/technologie", technologyRoutes);

app.listen(8000, () => {
  console.log("Serveur démarré sur http://localhost:8000");
});
